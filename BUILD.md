# Building the library for use with Trieng/Verso.

## Linux

```
sudo apt-get install freeglut3-dev pkg-config libxmu-dev libxi-dev libusb-1.0-0-dev
```

```
mkdir build
cd build
cmake -DBUILD_AUDIO=true ..
make
```

## Windows

```
mkdir build
cd build
cmake -DBUILD_AUDIO=true ..
open project with MSVC
```

